A simple REST API with Symfony2 and the FOSRestBundle for my very own training purposes.


- The API knows 2 Entities: Football clubs and football players in a one to many relationship.
- Simple CRUD operations are possible, nothing too fancy (yet).
