<?php

namespace Daniel\RestBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\Rest\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use Daniel\RestBundle\Entity\Club;
use Daniel\RestBundle\Entity\Player;
use Daniel\RestBundle\Form\PlayerType;

class PlayerController extends FOSRestController implements ClassResourceInterface
{
    //""" GET """

    /**
     * Collection get action
     * @var Request $request
     * @var integer $clubId Id of the club
     * @return array
     *
     * @Rest\View()
     */
    public function cgetAction(Request $request, $clubId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DanielRestBundle:Player')->findBy(
            array(
                'club' => $clubId,
            )
        );

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Get action
     * @var integer $clubId Id of the club
     * @var integer $id Id of the entity
     * @return array
     *
     * @Rest\View()
     */
    public function getAction($clubId, $id)
    {
        $entity = $this->getEntity($clubId, $id);

        return array(
            'entity' => $entity,
        );
    }

    //""" POST """

    /**
     * Collection post action
     * @var Request $request
     * @var integer $clubId Id of the club
     * @return View|array
     */
    public function cpostAction(Request $request, $clubId)
    {
        $organisation = $this->getClub($clubId);
        $entity = new Player();
        $entity->setClub($organisation);
        $form = $this->createForm(new PlayerType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectView(
                $this->generateUrl(
                    'get_club_player',
                    array(
                        'clubId' => $entity->getClub()->getId(),
                        'id' => $entity->getId()
                    )
                ),
                Codes::HTTP_CREATED
            );
        }

        return array(
            'form' => $form,
        );
    }

    //""" PUT """

    /**
     * Put action
     * @var Request $request
     * @var integer $clubId Id of the club
     * @var integer $id Id of the entity
     * @return View|array
     */
    public function putAction(Request $request, $clubId, $id)
    {
        $entity = $this->getEntity($clubId, $id);
        $form = $this->createForm(new PlayerType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->view(null, Codes::HTTP_NO_CONTENT);
        }

        return array(
            'form' => $form,
        );
    }

    //""" DELETE """

    /**
     * Delete action
     * @var integer $clubId Id of the club
     * @var integer $id Id of the entity
     * @return View
     */
    public function deleteAction($clubId, $id)
    {
        $entity = $this->getEntity($clubId, $id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return $this->view(null, Codes::HTTP_NO_CONTENT);
    }

    /**
     * Get entity instance
     * @var integer $clubId Id of the club
     * @var integer $id Id of the entity
     * @return Player
     */
    protected function getEntity($clubId, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DanielRestBundle:Player')->findOneBy(
            array(
                'id' => $id,
                'club' => $clubId,
            )
        );

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find player');
        }

        return $entity;
    }

    /**
     * Get club instance
     * @var integer $id Id of the club
     * @return Club
     */
    protected function getClub($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DanielRestBundle:Organisation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find club');
        }

        return $entity;
    }









}
