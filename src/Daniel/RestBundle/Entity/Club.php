<?php

namespace Daniel\RestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\SerializerBundle;
use Symfony\Component\Validator\Constraints;

/**
 * Club
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Daniel\RestBundle\Entity\ClubRepository")
 *
 * @ExclusionPolicy("all")
 */
class Club
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Player", mappedBy="club")
     *
     * @var Doctrine\Common\Collections\Collection $players
     *
     * @Expose
     */
    private $players;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     *
     * @Expose
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Club
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add players
     *
     * @param \Daniel\RestBundle\Entity\Player $players
     * @return Club
     */
    public function addPlayer(\Daniel\RestBundle\Entity\Player $players)
    {
        $this->players[] = $players;
    
        return $this;
    }

    /**
     * Remove players
     *
     * @param \Daniel\RestBundle\Entity\Player $players
     */
    public function removePlayer(\Daniel\RestBundle\Entity\Player $players)
    {
        $this->players->removeElement($players);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlayers()
    {
        return $this->players;
    }
}