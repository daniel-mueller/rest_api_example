<?php

namespace Daniel\RestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\SerializerBundle;

/**
 * Player
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Daniel\RestBundle\Entity\PlayerRepository")
 *
 * @ExclusionPolicy("all")
 */
class Player
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Club", inversedBy="player")
     *
     * @var Club $club
     */
    private $club;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     *
     * @Expose
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Player
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set club
     *
     * @param \Daniel\RestBundle\Entity\Club $club
     * @return Player
     */
    public function setClub(\Daniel\RestBundle\Entity\Club $club = null)
    {
        $this->club = $club;
    
        return $this;
    }

    /**
     * Get club
     *
     * @return \Daniel\RestBundle\Entity\Club 
     */
    public function getClub()
    {
        return $this->club;
    }
}